import React, { useState } from 'react'
import { useQuery } from 'react-query'
import { API } from './constants'

const FilterExample = () => {
  const onInputChange = (inputEvent) => setInputValue(inputEvent.target.value);
  const fetchFunc = ({ queryKey }) =>
    fetch(`${API}/posts?userId=${queryKey[1].userId}`).then((res) =>
      res.json()
    );

  const [inputValue, setInputValue] = useState(null);
  const { data, isSuccess, isLoading, isError } = useQuery(
    ['filterExample', { userId: inputValue }],
    fetchFunc
  );

  return (
    <>
      <h1 className="heading">Filter Example</h1>

      <div>
        <label>
          <p>Enter user ID:</p>
          <input type="text" onChange={onInputChange} />
        </label>

        {!inputValue && <p>No data</p>}
        {isError && <p>Error</p>}
        {isLoading && <p>Loading...</p>}
        {isSuccess &&
          data.map((item, index) => (
            <p key={item.id}>{`${index}. ${item.title}`}</p>
          ))}
      </div>
    </>
  )
}

export default FilterExample