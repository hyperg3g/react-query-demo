import './App.css';
import BasicExample from './BasicExample';
import PaginationExample from './PaginationExample';
import MutationsExample from './MutationsExample';
import FilterExample from './FilterExample';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'


function App() {
  const queryClient = new QueryClient()

  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <div className="app">
          <div className="links">
            <Link to="/">Basic Example</Link>
            <Link to="/filter">Filter</Link>
            <Link to="/pagination">Pagination</Link>
            <Link to="/mutations">Mutations</Link>
          </div>

          <Switch>
            <Route path="/filter">
              <FilterExample />
            </Route>
            <Route path="/mutations">
              <MutationsExample />
            </Route>
            <Route path="/pagination">
              <PaginationExample />
            </Route>
            <Route path="/">
              <BasicExample />
            </Route>
          </Switch>
        </div>
      </Router>

      <ReactQueryDevtools />
    </QueryClientProvider>
  );
}

export default App;
