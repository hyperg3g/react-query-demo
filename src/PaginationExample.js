import React, { useState } from 'react'
import { useQuery } from 'react-query'
import { SWAPI } from './constants'


const PaginationExample = () => {
  const fetchPlanets = ({ queryKey }) => fetch(`${SWAPI}/planets/?page=${queryKey[1].page}`).then(response => response.json())

  const [page, setPage] = useState(1)

  const { data, isSuccess, isLoading, isError, isFetching } = useQuery(['paginationExample', { page }], fetchPlanets, { keepPreviousData: true })

  console.log(data)

  return (
    <>
      <h1 className="heading">Pagination Example</h1>

      {isError && <p>Error</p>}
      {(isLoading || isFetching) && <p>Loading...</p>}
      {
        isSuccess && !isLoading && !isFetching &&
        data.results.map((item, index) => (
          <p key={index}>{`${++index}. ${item.name}`}</p>
        ))
      }

      <span>Current Page: {page}</span>
      <br />
      <button onClick={() => setPage(old => old - 1)} disabled={page === 1}>Previous Page</button>
      <button onClick={() => setPage(old => old + 1)} disabled={!data.next}>Next Page</button>
    </>
  )
}

export default PaginationExample