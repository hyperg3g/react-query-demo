import React from 'react'
import { useQuery } from 'react-query'
import { API } from './constants'


const BasicExample = () => {
  const fetchBasicExample = () => fetch(`${API}/posts`).then(response => response.json())

  const { data, isLoading, isSuccess, refetch } = useQuery('basicExample', fetchBasicExample)

  return (
    <>
      <h1 className="heading">Basic Example</h1>
      <button type="button" onClick={refetch}>Refetch data!</button>

      <div>
        {isLoading && <p>Loading . . .</p>}
        {isSuccess && data.map((item, index) => <p key={index}>{`${index + 1}. ${item.title}`}</p>)}
      </div>
    </>
  )
}

export default BasicExample